package postgre_model

import (
	"golang.org/x/net/context"
	"time"
	"wallet-service/pkg/common/db"
	pbUser "wallet-service/pkg/proto/user"
)

var postgreHandler db.PostgreHandler

func UserCreate(pbUser *pbUser.CreateUserRequest) error {
	postgreHandler.Connect()
	defer postgreHandler.Close()

	_, err := postgreHandler.DBPool.Exec(context.Background(), "INSERT INTO USERS (name, gender,mobile, email, birth,create_time) VALUES ($1, $2, $3, $4, $5, $6)", pbUser.Data.Name, pbUser.Data.Gender, pbUser.Data.Mobile, pbUser.Data.Email, pbUser.Data.Birth, time.Now())
	return err
}
