package main

import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"wallet-service/pkg/proto/user"
)

func main() {
	fmt.Println("Client")
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	defer cc.Close()
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}

	c := pbUser.NewUserClient(cc)

	user := pbUser.CreateUserRequest{
		Data: &pbUser.UserInfo{
			Name:   "Petar",
			Gender: 0,
			Mobile: "123",
			Birth:  "123",
			Email:  "petar",
		},
		Token: "1",
	}
	response, err := c.CreateUser(context.Background(), &user)

	if err != nil {
		log.Fatalf("Error %v", err)
	}

	fmt.Println(response)
}
