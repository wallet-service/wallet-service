package db

import (
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
)

type Configurations struct {
	Database PostgresDBConfiguration
}

type PostgresDBConfiguration struct {
	DBPostgresHost         string
	DBPostgresUser         string
	DBPostgresPassword     string
	DBPostgresDatabaseName string
	DBPostgresPort         string
}

func (config *Configurations) getConf() {
	viper.SetConfigName("config")
	viper.AddConfigPath("./pkg/common/config")
	viper.AutomaticEnv()
	viper.SetConfigType("yml")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	err := viper.Unmarshal(&config)
	if err != nil {
		panic(err)
	}
}

type PostgreHandler struct {
	DBPool *pgxpool.Pool
}

func (postgresHandler *PostgreHandler) Connect() {
	if postgresHandler.DBPool == nil {
		conf := &Configurations{}
		conf.getConf()

		dbUrl := fmt.Sprintf("postgres://%s:%s@%s:"+
			"%s/%s",
			conf.Database.DBPostgresUser, conf.Database.DBPostgresPassword, conf.Database.DBPostgresHost, conf.Database.DBPostgresPort, conf.Database.DBPostgresDatabaseName)
		conn, err := pgxpool.Connect(context.Background(), dbUrl)
		if err != nil {
			panic(err)
		}

		postgresHandler.DBPool = conn
	}
}

func (postgresHandler PostgreHandler) Close() {
	postgresHandler.DBPool.Close()
	postgresHandler.DBPool = nil
}
