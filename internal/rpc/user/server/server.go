package main

import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"log"
	"net"
	"os"
	"os/signal"
	"wallet-service/pkg/common/db/postgre_model"
	"wallet-service/pkg/proto/user"
)

type server struct {
}

func (s *server) CreateUser(_ context.Context, request *pbUser.CreateUserRequest) (*pbUser.CommonResponse, error) {
	err := postgre_model.UserCreate(request)

	if err != nil {
		return &pbUser.CommonResponse{
			ErrorCode: int32(codes.Internal),
			ErrorMsg:  "Error creating user",
		}, err
	}
	return &pbUser.CommonResponse{
		ErrorCode: int32(codes.OK),
		ErrorMsg:  "Created",
	}, nil
}

func (s *server) GetUserInfo(ctx context.Context, request *pbUser.GetUserInfoRequest) (*pbUser.GetUserInfoResponse, error) {
	panic("implement me")
}

func (s *server) UpdateUserInfo(ctx context.Context, request *pbUser.UpdateUserInfoRequest) (*pbUser.CommonResponse, error) {
	panic("implement me")
}

func (s *server) GetAllUsersUid(ctx context.Context, request *pbUser.GetAllUsersUidRequest) (*pbUser.GetAllUsersUidResponse, error) {
	panic("implement me")
}

func (s *server) DeleteUsers(ctx context.Context, request *pbUser.DeleteUsersRequest) (*pbUser.DeleteUsersResponse, error) {
	panic("implement me")
}

func main() {
	//if we crash we get the file name and line code
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	fmt.Println("pbUser Server started")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed %v", err)
	}

	var opts []grpc.ServerOption
	s := grpc.NewServer(opts...)

	pbUser.RegisterUserServer(s, &server{})

	go func() {
		fmt.Println("Starting server...")
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to serve: %v", err)
		}
	}()

	//Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	//Block until signal
	<-ch
	fmt.Println("Stopping server")
	s.Stop()
	fmt.Println("Stopping listener")
	lis.Close()
}
